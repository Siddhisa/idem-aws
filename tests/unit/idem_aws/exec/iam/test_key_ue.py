import pgpy
from pgpy.constants import CompressionAlgorithm
from pgpy.constants import HashAlgorithm
from pgpy.constants import KeyFlags
from pgpy.constants import PubKeyAlgorithm
from pgpy.constants import SymmetricKeyAlgorithm


async def test_list_filtering(hub, mock_hub, ctx):
    """
    The AWS API doesn't allow filtering by access_key_id or status;
     this test basically does combinatorial checks to make sure the
     filtering is working correctly.
    """
    mock_hub.exec.aws.iam.access_key.list = hub.exec.aws.iam.access_key.list
    mock_hub.exec.boto3.client.iam.list_access_keys.return_value = {
        "result": True,
        "ret": {
            "AccessKeyMetadata": [
                {"UserName": "tuser", "AccessKeyId": "KEYID1", "Status": "Active"},
                {"UserName": "tuser", "AccessKeyId": "KEYID2", "Status": "Active"},
                {"UserName": "tuser", "AccessKeyId": "KEYID3", "Status": "Inactive"},
                {},  # no response fields are required to be returned, do a test with the worst case
                {},
            ]
        },
        "comment": (),
        "ref": "client.iam.list._client_caller",
    }

    ret = await mock_hub.exec.aws.iam.access_key.list(ctx, user_name="tuser")
    assert len(ret["ret"]) == 5
    assert ret["result"], ret["comment"]

    ret = await mock_hub.exec.aws.iam.access_key.list(
        ctx, user_name="tuser", access_key_id="KEYID1"
    )
    assert len(ret["ret"]) == 1
    assert ret["ret"][0]["access_key_id"] == "KEYID1"
    assert ret["result"], ret["comment"]

    ret = await mock_hub.exec.aws.iam.access_key.list(
        ctx, user_name="tuser", access_key_id="KEYID2"
    )
    assert len(ret["ret"]) == 1
    assert ret["ret"][0]["access_key_id"] == "KEYID2"

    ret = await mock_hub.exec.aws.iam.access_key.list(
        ctx, user_name="tuser", status="Active"
    )
    assert len(ret["ret"]) == 2
    assert ret["result"], ret["comment"]

    ret = await mock_hub.exec.aws.iam.access_key.list(
        ctx, user_name="tuser", status="Inactive"
    )
    assert len(ret["ret"]) == 1

    ret = await mock_hub.exec.aws.iam.access_key.list(
        ctx, user_name="tuser", access_key_id="DNE"
    )
    assert ret["result"], "No matching key should be a valid result"
    assert len(ret["ret"]) == 0

    # invalid status
    ret = await mock_hub.exec.aws.iam.access_key.list(ctx, user_name="tuser", status="")
    assert ret["result"] is False


async def test_list_failed(hub, mock_hub, ctx):
    mock_hub.exec.aws.iam.access_key.list = hub.exec.aws.iam.access_key.list
    mock_hub.exec.boto3.client.iam.list_access_keys.return_value = {
        "result": False,
        "ret": None,
        "comment": (
            "NoSuchEntityException: An error occurred (NoSuchEntity) when calling the ListAccessKeys operation: The user with name HOO cannot be found.",
        ),
        "ref": "client.iam.list._client_caller",
    }
    ret = await mock_hub.exec.aws.iam.access_key.list(ctx, user_name="tuser")
    assert ret["result"] is False
    assert "Could not list access keys" in str(ret["comment"])


async def test_update_failed(hub, mock_hub, ctx):
    mock_hub.exec.aws.iam.access_key.update = hub.exec.aws.iam.access_key.update
    mock_hub.exec.boto3.client.iam.update_access_key.return_value = {
        "result": False,
        "comment": ("a comment",),
    }
    ret = await mock_hub.exec.aws.iam.access_key.update(
        ctx, user_name="tuser", access_key_id="SOMEKEY", status="Active"
    )
    assert ret["result"] is False
    assert "Could not update" in str(ret["comment"])


async def test_update_invalid_status(hub, mock_hub, ctx):
    mock_hub.exec.aws.iam.access_key.update = hub.exec.aws.iam.access_key.update
    mock_hub.exec.boto3.client.iam.update_access_key.return_value = {
        "result": False,
        "comment": (
            "ClientError: An error occurred (ValidationError) when calling the UpdateAccessKey "
            "operation: 1 validation error detected: Value 'NOSUCHSTATUS' at 'status' failed to "
            "satisfy constraint: Member must satisfy enum value set: [Active, Inactive]",
        ),
    }
    ret = await mock_hub.exec.aws.iam.access_key.update(
        ctx,
        user_name="tuser",
        access_key_id="KEYID1",
        status="NOSUCHSTATUS",
    )
    assert ret["result"] is False
    assert "Could not update" in str(ret["comment"])


async def test_delete_failed(hub, mock_hub, ctx):
    mock_hub.exec.aws.iam.access_key.delete = hub.exec.aws.iam.access_key.delete
    mock_hub.exec.boto3.client.iam.delete_access_key.return_value = {
        "result": False,
        "comment": ("a comment",),
    }
    ret = await mock_hub.exec.aws.iam.access_key.delete(
        ctx, user_name="tuser", access_key_id="SOMEKEY"
    )
    assert ret["result"] is False
    assert "Could not delete" in str(ret["comment"])


async def test_create_failed(hub, mock_hub, ctx):
    mock_hub.exec.aws.iam.access_key.create = hub.exec.aws.iam.access_key.create
    mock_hub.exec.boto3.client.iam.create_access_key.return_value = {
        "result": False,
        "ret": None,
        "comment": ("failed to create access_key",),
    }
    ret = await mock_hub.exec.aws.iam.access_key.create(ctx, user_name="tuser")
    assert ret["result"] is False


async def test_create_pgp(hub, mock_hub, ctx):
    mock_hub.exec.aws.iam.access_key.create = hub.exec.aws.iam.access_key.create
    mock_hub.tool.aws.b64.decode = hub.tool.aws.b64.decode
    mock_hub.tool.aws.b64.encode = hub.tool.aws.b64.encode

    pgp_key = pgpy.PGPKey.new(PubKeyAlgorithm.RSAEncryptOrSign, 2048)
    uid = pgpy.PGPUID.new("tuser", email="tuser@example.com")
    pgp_key.add_uid(
        uid,
        usage={KeyFlags.Sign, KeyFlags.EncryptCommunications, KeyFlags.EncryptStorage},
        hashes=[
            HashAlgorithm.SHA256,
            HashAlgorithm.SHA384,
            HashAlgorithm.SHA512,
            HashAlgorithm.SHA224,
        ],
        ciphers=[
            SymmetricKeyAlgorithm.AES256,
            SymmetricKeyAlgorithm.AES192,
            SymmetricKeyAlgorithm.AES128,
        ],
        compression=[
            CompressionAlgorithm.ZLIB,
            CompressionAlgorithm.BZ2,
            CompressionAlgorithm.ZIP,
            CompressionAlgorithm.Uncompressed,
        ],
    )

    b64_public_key_str = hub.tool.aws.b64.encode(pgp_key.pubkey)

    secret_text = "THISISMYSECRETACCESSKEY"

    mock_hub.exec.boto3.client.iam.create_access_key.return_value = {
        "result": True,
        "ret": {
            "AccessKey": {
                "UserName": "tuser",
                "AccessKeyId": "AKIAX2FJ77DC11111111",
                "Status": "Active",
                "SecretAccessKey": secret_text,
            },
            "ResponseMetadata": {},
        },
    }

    ret = await mock_hub.exec.aws.iam.access_key.create(
        ctx, user_name="tuser", pgp_key=b64_public_key_str
    )
    assert ret["result"], ret["comment"]
    b64_secret_key = ret["ret"]["secret_access_key"]
    secret_key = hub.tool.aws.b64.decode(b64_secret_key)
    secret_key_message = pgpy.PGPMessage.from_blob(secret_key)
    pgp_key.decrypt(secret_key_message)
    assert secret_key_message.message == secret_text
