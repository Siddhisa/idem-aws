def test_validation_rec(hub, mock_hub):
    mock_hub.states.aws.acm.certificate_validation.DNS = (
        hub.states.aws.acm.certificate_validation.DNS
    )
    mock_hub.tool.aws.acm.certificate_validation_utils.validation_rec = (
        hub.tool.aws.acm.certificate_validation_utils.validation_rec
    )
    # Case 1: When validation_record_fqdns are present for all validation options and method for validation is DNS validation
    old_certificate = {
        "DomainValidationOptions": [
            {
                "DomainName": "edu.com",
                "Validationdomain": "edu.com",
                "ValidationMethod": "DNS",
                "ResourceRecord": {
                    "Name": "_1650742e08eec3b28f5d7f0cf24fddae.edu.com.",
                    "Type": "CNAME",
                    "Value": "_2c5945df20220a0f518c2e857cb97373.mjclfywhbs.acm-validations.aws.",
                },
                "validation_status": "PENDING_VALIDATION",
            },
            {
                "DomainName": "testing.edu.com",
                "Validationdomain": "testing.edu.com",
                "ValidationMethod": "DNS",
                "ResourceRecord": {
                    "Name": "_8654d43a111c4fef1993a2088182e067.testing.edu.com.",
                    "Type": "CNAME",
                    "Value": "_deb428c58b2e9bbd835d772bc3d37d34.gtjzbhhwqc.acm-validations.aws.",
                },
                "validation_status": "PENDING_VALIDATION",
            },
        ],
    }
    validation_record_fqdns = [
        "_1650742e08eec3b28f5d7f0cf24fddae.edu.com.",
        "_8654d43a111c4fef1993a2088182e067.testing.edu.com.",
    ]

    validity_of_domain = (
        mock_hub.tool.aws.acm.certificate_validation_utils.validation_rec(
            validation_rec_fqdns=validation_record_fqdns,
            certificate=old_certificate,
        )
    )
    assert validity_of_domain["result"] == True
    assert not validity_of_domain["comment"]

    # Case 2: When method of validation is E-mail validation
    old_certificate = {
        "DomainValidationOptions": [
            {
                "DomainName": "edu.com",
                "Validationdomain": "edu.com",
                "ValidationMethod": "EMAIL",
                "ResourceRecord": {
                    "Name": "_1650742e08eec3b28f5d7f0cf24fddae.edu.com.",
                    "Type": "CNAME",
                    "Value": "_2c5945df20220a0f518c2e857cb97373.mjclfywhbs.acm-validations.aws.",
                },
                "validation_status": "PENDING_VALIDATION",
            },
            {
                "DomainName": "testing.edu.com",
                "Validationdomain": "testing.edu.com",
                "ValidationMethod": "EMAIL",
                "ResourceRecord": {
                    "Name": "_8654d43a111c4fef1993a2088182e067.testing.edu.com.",
                    "Type": "CNAME",
                    "Value": "_deb428c58b2e9bbd835d772bc3d37d34.gtjzbhhwqc.acm-validations.aws.",
                },
                "validation_status": "PENDING_VALIDATION",
            },
        ],
    }
    validation_record_fqdns = [
        "_1650742e08eec3b28f5d7f0cf24fddae.edu.com.",
        "_8654d43a111c4fef1993a2088182e067.testing.edu.com.",
    ]

    validity_of_domain = (
        mock_hub.tool.aws.acm.certificate_validation_utils.validation_rec(
            validation_rec_fqdns=validation_record_fqdns,
            certificate=old_certificate,
        )
    )
    assert validity_of_domain["result"] == False
    assert (
        "validation_record_fqdns is only valid for DNS validation"
        in validity_of_domain["comment"]
    )

    # Case 3: When one of the validation_record_fqdns is missing
    old_certificate = {
        "DomainValidationOptions": [
            {
                "DomainName": "edu.com",
                "Validationdomain": "edu.com",
                "ValidationMethod": "DNS",
                "ResourceRecord": {
                    "Name": "_1650742e08eec3b28f5d7f0cf24fddae.edu.com.",
                    "Type": "CNAME",
                    "Value": "_2c5945df20220a0f518c2e857cb97373.mjclfywhbs.acm-validations.aws.",
                },
                "validation_status": "PENDING_VALIDATION",
            },
            {
                "DomainName": "testing.edu.com",
                "Validationdomain": "testing.edu.com",
                "ValidationMethod": "DNS",
                "ResourceRecord": {
                    "Name": "_8654d43a111c4fef1993a2088182e067.testing.edu.com.",
                    "Type": "CNAME",
                    "Value": "_deb428c58b2e9bbd835d772bc3d37d34.gtjzbhhwqc.acm-validations.aws.",
                },
                "validation_status": "PENDING_VALIDATION",
            },
        ],
    }
    validation_record_fqdns = ["_1650742e08eec3b28f5d7f0cf24fddae.edu.com."]

    validity_of_domain = (
        mock_hub.tool.aws.acm.certificate_validation_utils.validation_rec(
            validation_rec_fqdns=validation_record_fqdns,
            certificate=old_certificate,
        )
    )

    assert validity_of_domain["result"] == False
    assert "missing DNS validation record" in str(validity_of_domain["comment"])


def test_extract_validation_method(hub, mock_hub):
    mock_hub.tool.aws.acm.certificate_validation_utils.extract_validation_method = (
        hub.tool.aws.acm.certificate_validation_utils.extract_validation_method
    )
    mock_hub.states.aws.acm.certificate_validation.DNS = (
        hub.states.aws.acm.certificate_validation.DNS
    )
    mock_hub.states.aws.acm.certificate_validation.EMAIL = (
        hub.states.aws.acm.certificate_validation.EMAIL
    )
    # Case 1: When validation method is 'DNS'
    old_certificate = {
        "DomainValidationOptions": [
            {
                "DomainName": "edu.com",
                "Validationdomain": "edu.com",
                "ValidationMethod": "DNS",
                "ResourceRecord": {
                    "Name": "_1650742e08eec3b28f5d7f0cf24fddae.edu.com.",
                    "Type": "CNAME",
                    "Value": "_2c5945df20220a0f518c2e857cb97373.mjclfywhbs.acm-validations.aws.",
                },
                "validation_status": "PENDING_VALIDATION",
            },
            {
                "DomainName": "testing.edu.com",
                "Validationdomain": "testing.edu.com",
                "ValidationMethod": "DNS",
                "ResourceRecord": {
                    "Name": "_8654d43a111c4fef1993a2088182e067.testing.edu.com.",
                    "Type": "CNAME",
                    "Value": "_deb428c58b2e9bbd835d772bc3d37d34.gtjzbhhwqc.acm-validations.aws.",
                },
                "validation_status": "PENDING_VALIDATION",
            },
        ],
    }
    validation_method = (
        mock_hub.tool.aws.acm.certificate_validation_utils.extract_validation_method(
            old_certificate
        )
    )
    assert validation_method == hub.states.aws.acm.certificate_validation.DNS

    # Case 2: When validation method is 'EMAIL'
    old_certificate = {
        "DomainValidationOptions": [
            {
                "DomainName": "edu.com",
                "Validationdomain": "edu.com",
                "ValidationMethod": "EMAIL",
                "ResourceRecord": {
                    "Name": "_1650742e08eec3b28f5d7f0cf24fddae.edu.com.",
                    "Type": "CNAME",
                    "Value": "_2c5945df20220a0f518c2e857cb97373.mjclfywhbs.acm-validations.aws.",
                },
                "validation_status": "PENDING_VALIDATION",
            },
            {
                "DomainName": "testing.edu.com",
                "Validationdomain": "testing.edu.com",
                "ValidationMethod": "EMAIL",
                "ResourceRecord": {
                    "Name": "_8654d43a111c4fef1993a2088182e067.testing.edu.com.",
                    "Type": "CNAME",
                    "Value": "_deb428c58b2e9bbd835d772bc3d37d34.gtjzbhhwqc.acm-validations.aws.",
                },
                "validation_status": "PENDING_VALIDATION",
            },
        ],
    }
    validation_method = (
        mock_hub.tool.aws.acm.certificate_validation_utils.extract_validation_method(
            old_certificate
        )
    )
    assert validation_method == hub.states.aws.acm.certificate_validation.EMAIL
