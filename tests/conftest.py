import os
import pathlib

import pytest


# ================================================================================
# Pytest helpers
# ================================================================================
# TODO add some pytest features that skips the rest of the tests (besides test_delete)
#   when a module starting with "test_create" has test_create fail
def pytest_configure(config):
    config.addinivalue_line(
        "markers",
        "kubernetes: Mark test as needing a kubeconfig from idem-kubernetes",
    )
    config.addinivalue_line(
        "markers",
        "localstack(pro): Mark test to run only if localstack is configured",
    )


def _localstack_marker(item, mark):
    """
    If a test is marked with:
        @pytest.mark.localstack("localstack", pro=True)
    and the backend is "localstack", and LOCALSTACK_API_KEY isn't configured, then skip
    """
    # TODO do a request on the ctx.endpoint_url and see if it's headers look like localstack
    #   For now just see if a flag was set that tells pytest what endpoint is being used explicitly
    using_localstack_endpoint = True
    # If TEST_ON_REAL_AWS is true, then we assume that the test will be run on real aws endpoint,
    # regardless of localstack flags.
    if os.getenv("TEST_ON_REAL_AWS", "false").lower() == "true":
        using_localstack_endpoint = False

    if using_localstack_endpoint and not all(mark.args):
        # skip this test if an endpoint_url is configured and if it's headers don't look like localstack
        pytest.skip("This test does not run on localstack")

    pro_enabled = bool(os.environ.get("REQUIRE_PRO")) or bool(
        os.environ.get("LOCALSTACK_API_KEY")
    )
    needs_pro = mark.kwargs.get("pro", False)
    # If localstack is configured at the endpoint and localstack pro isn't configured then skip this test
    if using_localstack_endpoint:
        if needs_pro and not pro_enabled:
            pytest.skip(
                "Localstack pro needs to be configured with LOCALSTACK_API_KEY in the environment to run this test"
            )
        elif pro_enabled and not needs_pro:
            pytest.skip("This test does not run with localstack pro")


def _kubernetes_marker(item, mark):
    """
    If a test is marked with:
        @pytest.mark.kubernetes
    Then check for a kubernetes configuration via idem-kubernetes or KUBECONFIG
    """
    # TODO check for an acct configuration from idem-kube
    if not os.environ.get("KUBECONFIG"):
        pytest.skip("This test requires a kubernetes config")


def pytest_runtest_setup(item):
    for mark in item.iter_markers(name="localstack"):
        _localstack_marker(item, mark)
    for mark in item.iter_markers(name="kubernetes"):
        _kubernetes_marker(item, mark)


@pytest.fixture(scope="session")
def code_dir() -> pathlib.Path:
    return pathlib.Path(__file__).parent.parent.absolute()


# --------------------------------------------------------------------------------
