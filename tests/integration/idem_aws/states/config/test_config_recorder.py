import copy
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_config_recorder(hub, ctx):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    account_details = await hub.exec.boto3.client.sts.get_caller_identity(ctx)
    acct_num = account_details["ret"]["Account"]
    temp_name = "default"
    recording_group_to_add = {
        "allSupported": False,
        "includeGlobalResourceTypes": False,
        "resourceTypes": ["AWS::EC2::CustomerGateway", "AWS::EC2::EIP"],
    }
    recording_group_to_update = {
        "allSupported": False,
        "includeGlobalResourceTypes": False,
        "resourceTypes": ["AWS::EC2::Host", "AWS::EC2::Instance"],
    }
    role_arn = (
        "arn:aws:iam::"
        + acct_num
        + ":role/aws-service-role/config.amazonaws.com/AWSServiceRoleForConfig"
    )
    recording = False
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # create config-recorder with test flag
    ret = await hub.states.aws.config.config_recorder.present(
        test_ctx,
        name=temp_name,
        role_arn=role_arn,
        recording=recording,
        recording_group=recording_group_to_add,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.config.config_recorder '{temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert temp_name == resource.get("name")
    assert recording_group_to_add == resource.get("recording_group")
    assert role_arn == resource.get("role_arn")
    assert recording == resource.get("recording")

    # create config-recorder in real
    ret = await hub.states.aws.config.config_recorder.present(
        ctx,
        name=temp_name,
        role_arn=role_arn,
        recording=recording,
        recording_group=recording_group_to_add,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Created aws.config.config_recorder '{temp_name}'" in ret["comment"]
    resource = ret.get("new_state")
    resource_name = resource.get("name")
    assert recording_group_to_add == resource.get("recording_group")
    assert role_arn == resource.get("role_arn")
    assert recording == resource.get("recording")

    # describe config-recorder
    describe_ret = await hub.states.aws.config.config_recorder.describe(ctx)
    assert describe_ret
    assert "aws.config.config_recorder.present" in describe_ret.get(
        resource_name + "-config-recorder"
    )
    described_resource = describe_ret.get(resource_name + "-config-recorder").get(
        "aws.config.config_recorder.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "name" in described_resource_map
    assert temp_name == described_resource_map["name"]
    assert "resource_id" in described_resource_map
    assert temp_name == described_resource_map["resource_id"]
    assert "role_arn" in described_resource_map
    assert role_arn == described_resource_map["role_arn"]
    assert "recording" in described_resource_map
    assert recording == described_resource_map["recording"]
    assert "recording_group" in described_resource_map
    assert recording_group_to_add == described_resource_map["recording_group"]

    # Update config-recorder with test flag
    ret = await hub.states.aws.config.config_recorder.present(
        test_ctx,
        name=temp_name,
        role_arn=role_arn,
        recording=recording,
        resource_id=temp_name,
        recording_group=recording_group_to_update,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Would update aws.config.config_recorder '{temp_name}'" in ret["comment"]
    resource = ret.get("new_state")
    assert recording_group_to_update == resource.get("recording_group")
    assert role_arn == resource.get("role_arn")
    assert recording == resource.get("recording")

    # update config-recorder in real
    ret = await hub.states.aws.config.config_recorder.present(
        ctx,
        name=temp_name,
        role_arn=role_arn,
        recording=recording,
        resource_id=temp_name,
        recording_group=recording_group_to_update,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Updated aws.config.config_recorder '{temp_name}'" in ret["comment"]
    resource = ret.get("new_state")
    assert recording_group_to_update == resource.get("recording_group")
    assert role_arn == resource.get("role_arn")
    assert recording == resource.get("recording")

    # Delete config-recorder with test flag
    ret = await hub.states.aws.config.config_recorder.absent(
        test_ctx, name=temp_name, resource_id=temp_name
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert ret["old_state"]["name"] == resource_name
    assert ret["old_state"]["recording_group"] == resource.get("recording_group")
    assert (
        f"Would delete aws.config.config_recorder '{resource_name}'" in ret["comment"]
    )

    # Delete config-recorder in real
    ret = await hub.states.aws.config.config_recorder.absent(
        ctx, name=temp_name, resource_id=temp_name
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert ret["old_state"]["name"] == resource_name
    assert ret["old_state"]["recording_group"] == resource.get("recording_group")
    assert f"Deleted aws.config.config_recorder '{resource_name}'" in ret["comment"]

    # Deleting config-recorder again should be a no-op
    ret = await hub.states.aws.config.config_recorder.absent(
        ctx, name=temp_name, resource_id=temp_name
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"aws.config.config_recorder '{resource_name}' already absent" in ret["comment"]
    )
