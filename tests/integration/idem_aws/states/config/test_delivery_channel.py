import copy
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_delivery_channel(hub, ctx):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    account_details = await hub.exec.boto3.client.sts.get_caller_identity(ctx)
    acct_num = account_details["ret"]["Account"]
    temp_name = "delivery-channel"
    config_snapshot_delivery_properties_to_add = {"delivery_frequency": "One_Hour"}
    config_snapshot_delivery_properties_to_update = {
        "delivery_frequency": "Three_Hours"
    }
    s3_bucket_name = "config-bucket-" + acct_num
    sns_topic_arn = (
        "arn:aws:sns:"
        + ctx["acct"].get("region_name")
        + ":"
        + acct_num
        + ":my-test-topic"
    )
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # create delivery-channel with test flag
    ret = await hub.states.aws.config.delivery_channel.present(
        test_ctx,
        name=temp_name,
        s3_bucket_name=s3_bucket_name,
        sns_topic_arn=sns_topic_arn,
        config_snapshot_delivery_properties=config_snapshot_delivery_properties_to_add,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.config.delivery_channel '{temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert temp_name == resource.get("name")
    assert config_snapshot_delivery_properties_to_add.get(
        "delivery_frequency"
    ) == resource.get("configSnapshotDeliveryProperties").get("deliveryFrequency")
    assert s3_bucket_name == resource.get("s3BucketName")
    assert sns_topic_arn == resource.get("snsTopicARN")

    # Create config recorder before delivery channel
    recording_group_to_add = {
        "allSupported": False,
        "includeGlobalResourceTypes": False,
        "resourceTypes": ["AWS::EC2::CustomerGateway", "AWS::EC2::EIP"],
    }
    role_arn = (
        "arn:aws:iam::"
        + acct_num
        + ":role/aws-service-role/config.amazonaws.com/AWSServiceRoleForConfig"
    )
    recording = False
    ret = await hub.states.aws.config.config_recorder.present(
        ctx,
        name=temp_name,
        role_arn=role_arn,
        recording=recording,
        recording_group=recording_group_to_add,
    )

    # test in real
    ret = await hub.states.aws.config.delivery_channel.present(
        ctx,
        name=temp_name,
        s3_bucket_name=s3_bucket_name,
        sns_topic_arn=sns_topic_arn,
        config_snapshot_delivery_properties=config_snapshot_delivery_properties_to_add,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Created aws.config.delivery_channel '{temp_name}'" in ret["comment"]
    resource = ret.get("new_state")
    resource_name = resource.get("name")

    describe_ret = await hub.states.aws.config.delivery_channel.describe(ctx)
    assert describe_ret
    assert "aws.config.delivery_channel.present" in describe_ret.get(resource_name)
    described_resource = describe_ret.get(resource_name).get(
        "aws.config.delivery_channel.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "name" in described_resource_map
    assert "resource_id" in described_resource_map
    assert "sns_topic_arn" in described_resource_map
    assert "config_snapshot_delivery_properties" in described_resource_map
    assert temp_name == described_resource_map["resource_id"]

    # Update delivery-channel with test flag
    ret = await hub.states.aws.config.delivery_channel.present(
        test_ctx,
        name=temp_name,
        s3_bucket_name=s3_bucket_name,
        sns_topic_arn=sns_topic_arn,
        config_snapshot_delivery_properties=config_snapshot_delivery_properties_to_update,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Would update aws.config.delivery_channel '{temp_name}'" in ret["comment"]
    resource = ret.get("new_state")
    assert temp_name == resource.get("name")
    assert config_snapshot_delivery_properties_to_update.get(
        "delivery_frequency"
    ) == resource.get("configSnapshotDeliveryProperties").get("deliveryFrequency")
    assert s3_bucket_name == resource.get("s3BucketName")
    assert sns_topic_arn == resource.get("snsTopicARN")

    # update delivery-channel in real
    ret = await hub.states.aws.config.delivery_channel.present(
        ctx,
        name=temp_name,
        s3_bucket_name=s3_bucket_name,
        sns_topic_arn=sns_topic_arn,
        config_snapshot_delivery_properties=config_snapshot_delivery_properties_to_update,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Updated aws.config.delivery_channel '{temp_name}'" in ret["comment"]
    resource = ret.get("new_state")

    # Delete delivery-channel with test flag
    ret = await hub.states.aws.config.delivery_channel.absent(
        test_ctx, name=resource_name
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert resource_name == ret["old_state"]["name"]
    assert s3_bucket_name == ret["old_state"]["s3_bucket_name"]
    assert sns_topic_arn == ret["old_state"]["sns_topic_arn"]

    assert (
        f"Would delete aws.config.delivery_channel '{resource_name}'" in ret["comment"]
    )

    # Delete delivery-channel in real
    ret = await hub.states.aws.config.delivery_channel.absent(ctx, name=resource_name)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert ret["old_state"]["name"] == resource_name
    assert ret["old_state"]["s3_bucket_name"] == s3_bucket_name
    assert ret["old_state"]["sns_topic_arn"] == sns_topic_arn

    assert f"Deleted aws.config.delivery_channel '{resource_name}'" in ret["comment"]

    # Deleting delivery-channel again should be a no-op
    ret = await hub.states.aws.config.delivery_channel.absent(ctx, name=resource_name)
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"aws.config.delivery_channel '{resource_name}' already absent"
        in ret["comment"]
    )

    # delete the config recorder to clean up stuff
    ret = await hub.states.aws.config.config_recorder.absent(
        ctx, name=temp_name, resource_id=temp_name
    )
