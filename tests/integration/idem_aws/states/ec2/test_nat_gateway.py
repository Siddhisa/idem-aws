import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_nat_gateway(hub, ctx, aws_ec2_subnet):
    # Create nat_gateway
    nat_gateway_name = "idem-test-nat-gateway-" + str(uuid.uuid4())
    tags = {"Name": nat_gateway_name}
    # Create nat_gateway with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.ec2.nat_gateway.present(
        test_ctx,
        name=nat_gateway_name,
        subnet_id=aws_ec2_subnet.get("SubnetId"),
        connectivity_type="private",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.ec2.nat_gateway '{nat_gateway_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert nat_gateway_name == resource.get("name")
    assert aws_ec2_subnet.get("SubnetId") == resource.get("subnet_id")
    assert "private" == resource.get("connectivity_type")
    assert tags == resource.get("tags")

    ret = await hub.states.aws.ec2.nat_gateway.present(
        ctx,
        name=nat_gateway_name,
        subnet_id=aws_ec2_subnet.get("SubnetId"),
        connectivity_type="private",
        tags=tags,
    )
    assert not ret.get("old_state") and ret.get("new_state")
    assert ret["result"], ret["comment"]
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    assert f"Created '{resource_id}'" in ret["comment"]
    assert nat_gateway_name == resource.get("name")
    assert aws_ec2_subnet.get("SubnetId") == resource.get("subnet_id")
    assert "private" == resource.get("connectivity_type")
    assert tags == resource.get("tags")

    # verify that created nat_gateway is present
    describe_ret = await hub.states.aws.ec2.nat_gateway.describe(ctx)
    assert resource_id in describe_ret
    assert "aws.ec2.nat_gateway.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.ec2.nat_gateway.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert aws_ec2_subnet.get("SubnetId") == described_resource_map.get("subnet_id")
    assert tags == described_resource_map.get("tags")
    assert "private" == described_resource_map.get("connectivity_type")

    # Update tags for nat gateway with test flag
    tags.update(
        {
            f"idem-test-nat_gateway-key-{str(uuid.uuid4())}": f"idem-test-nat_gateway-value-{str(uuid.uuid4())}",
        }
    )
    ret = await hub.states.aws.ec2.nat_gateway.present(
        test_ctx,
        name=nat_gateway_name,
        subnet_id=aws_ec2_subnet.get("SubnetId"),
        resource_id=resource_id,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Would update tags" in str(ret["comment"])
    resource = ret.get("new_state")
    assert tags == resource.get("tags")

    # update tags
    new_tags = {"new-name": resource_id}

    ret = await hub.states.aws.ec2.nat_gateway.present(
        ctx,
        name=nat_gateway_name,
        resource_id=resource_id,
        subnet_id=aws_ec2_subnet.get("SubnetId"),
        connectivity_type="private",
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    updated_tags = resource.get("tags")
    assert updated_tags
    assert 1 == len(updated_tags)
    assert new_tags == updated_tags

    # Delete nat_gateway with test flag
    ret = await hub.states.aws.ec2.nat_gateway.absent(
        test_ctx, name=nat_gateway_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.ec2.nat_gateway {nat_gateway_name}" in ret["comment"]

    # Delete instance
    ret = await hub.states.aws.ec2.nat_gateway.absent(
        ctx, name=nat_gateway_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and (not ret.get("new_state"))
    assert f"Deleted '{nat_gateway_name}'" in ret["comment"]

    # Deleting the same instance again (deleted state) will not invoke delete on AWS side.
    ret = await hub.states.aws.ec2.nat_gateway.absent(
        ctx, name=nat_gateway_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (
        f"aws.ec2.nat_gateway '{nat_gateway_name}' is in deleted state."
        in ret["comment"]
    )
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
