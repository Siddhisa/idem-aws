import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_api(hub, ctx):
    api_temp_name = "idem-test-api-" + str(uuid.uuid4())
    protocol_type = "HTTP"
    cors_configuration = {"AllowCredentials": True, "MaxAge": 60}
    new_cors_configuration = {"AllowCredentials": False, "MaxAge": 120}
    description = "Idem integration test - " + api_temp_name
    new_description = "Idem integration test - new - " + api_temp_name
    disable_execute_api_endpoint = True
    new_disable_execute_api_endpoint = False
    route_selection_expression = "$request.method $request.path"
    tags = {"idem-resource-name": api_temp_name}
    new_tags = {"idem-resource-name-new": "new-" + api_temp_name}
    version = "v1"
    new_version = "v2"

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create apigatewayv2 api with test flag
    ret = await hub.states.aws.apigatewayv2.api.present(
        test_ctx,
        name=api_temp_name,
        protocol_type=protocol_type,
        cors_configuration=cors_configuration,
        description=description,
        disable_execute_api_endpoint=disable_execute_api_endpoint,
        route_selection_expression=route_selection_expression,
        tags=tags,
        version=version,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.apigatewayv2.api", name=api_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert api_temp_name == resource.get("name")
    assert protocol_type == resource.get("protocol_type")
    assert cors_configuration == resource.get("cors_configuration")
    assert description == resource.get("description")
    assert disable_execute_api_endpoint == resource.get("disable_execute_api_endpoint")
    assert route_selection_expression == resource.get("route_selection_expression")
    assert tags == resource.get("tags")
    assert version == resource.get("version")

    # Create apigatewayv2 api
    ret = await hub.states.aws.apigatewayv2.api.present(
        ctx,
        name=api_temp_name,
        protocol_type=protocol_type,
        cors_configuration=cors_configuration,
        description=description,
        disable_execute_api_endpoint=disable_execute_api_endpoint,
        route_selection_expression=route_selection_expression,
        tags=tags,
        version=version,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.apigatewayv2.api", name=api_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert api_temp_name == resource.get("name")
    assert protocol_type == resource.get("protocol_type")
    assert cors_configuration == resource.get("cors_configuration")
    assert description == resource.get("description")
    assert disable_execute_api_endpoint == resource.get("disable_execute_api_endpoint")
    assert route_selection_expression == resource.get("route_selection_expression")
    assert tags == resource.get("tags")
    assert version == resource.get("version")

    resource_id = resource.get("resource_id")

    # Verify that the created apigatewayv2 api is present (describe)
    ret = await hub.states.aws.apigatewayv2.api.describe(ctx)

    assert api_temp_name in ret
    assert "aws.apigatewayv2.api.present" in ret.get(api_temp_name)
    resource = ret.get(api_temp_name).get("aws.apigatewayv2.api.present")
    resource_map = dict(ChainMap(*resource))
    assert resource_id == resource_map.get("resource_id")
    assert api_temp_name == resource_map.get("name")
    assert protocol_type == resource_map.get("protocol_type")
    assert cors_configuration == resource_map.get("cors_configuration")
    assert description == resource_map.get("description")
    assert disable_execute_api_endpoint == resource_map.get(
        "disable_execute_api_endpoint"
    )
    assert route_selection_expression == resource_map.get("route_selection_expression")
    assert tags == resource_map.get("tags")
    assert version == resource_map.get("version")

    # Create apigatewayv2 api again with same resource_id and no change in state with test flag
    ret = await hub.states.aws.apigatewayv2.api.present(
        test_ctx,
        name=api_temp_name,
        resource_id=resource_id,
        protocol_type=protocol_type,
        cors_configuration=cors_configuration,
        description=description,
        disable_execute_api_endpoint=disable_execute_api_endpoint,
        route_selection_expression=route_selection_expression,
        tags=tags,
        version=version,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.api",
            name=api_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Create apigatewayv2 api again with same resource_id and no change in state
    ret = await hub.states.aws.apigatewayv2.api.present(
        ctx,
        name=api_temp_name,
        resource_id=resource_id,
        protocol_type=protocol_type,
        cors_configuration=cors_configuration,
        description=description,
        disable_execute_api_endpoint=disable_execute_api_endpoint,
        route_selection_expression=route_selection_expression,
        tags=tags,
        version=version,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.api",
            name=api_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Update apigatewayv2 api with test flag
    ret = await hub.states.aws.apigatewayv2.api.present(
        test_ctx,
        name=api_temp_name,
        resource_id=resource_id,
        protocol_type=protocol_type,
        cors_configuration=new_cors_configuration,
        description=new_description,
        disable_execute_api_endpoint=new_disable_execute_api_endpoint,
        route_selection_expression=route_selection_expression,
        tags=new_tags,
        version=new_version,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.api",
            name=api_temp_name,
        )[0]
        in ret["comment"]
    )
    assert (
        f"Would update parameters: cors_configuration,description,disable_execute_api_endpoint,version"
        in ret["comment"]
    )
    assert (
        f"Would update tags: Add {list(new_tags.keys())} Remove {list(tags.keys())}"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert api_temp_name == resource.get("name")
    assert protocol_type == resource.get("protocol_type")
    assert new_cors_configuration == resource.get("cors_configuration")
    assert new_description == resource.get("description")
    assert new_disable_execute_api_endpoint == resource.get(
        "disable_execute_api_endpoint"
    )
    assert route_selection_expression == resource.get("route_selection_expression")
    assert new_tags == resource.get("tags")
    assert new_version == resource.get("version")

    # Update apigatewayv2 api
    ret = await hub.states.aws.apigatewayv2.api.present(
        ctx,
        name=api_temp_name,
        resource_id=resource_id,
        protocol_type=protocol_type,
        cors_configuration=new_cors_configuration,
        description=new_description,
        disable_execute_api_endpoint=new_disable_execute_api_endpoint,
        route_selection_expression=route_selection_expression,
        tags=new_tags,
        version=new_version,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.api",
            name=api_temp_name,
        )[0]
        in ret["comment"]
    )
    assert (
        f"Updated parameters: cors_configuration,description,disable_execute_api_endpoint,version"
        in ret["comment"]
    )
    assert (
        f"Updated tags: Add {list(new_tags.keys())} Remove {list(tags.keys())}"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert api_temp_name == resource.get("name")
    assert protocol_type == resource.get("protocol_type")
    assert new_cors_configuration == resource.get("cors_configuration")
    assert new_description == resource.get("description")
    assert new_disable_execute_api_endpoint == resource.get(
        "disable_execute_api_endpoint"
    )
    assert route_selection_expression == resource.get("route_selection_expression")
    if hub.tool.utils.is_running_localstack(ctx):
        # LocalStack does not have support for apigatewayv2 tags
        assert tags == resource.get("tags")
    else:
        assert new_tags == resource.get("tags")
    assert new_version == resource.get("version")

    # Search for existing apigatewayv2 api
    ret = await hub.states.aws.apigatewayv2.api.search(
        ctx,
        name=api_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert api_temp_name == resource.get("name")
    assert resource_id == resource.get("resource_id")
    assert protocol_type == resource.get("protocol_type")
    assert new_cors_configuration == resource.get("cors_configuration")
    assert new_description == resource.get("description")
    assert new_disable_execute_api_endpoint == resource.get(
        "disable_execute_api_endpoint"
    )
    assert route_selection_expression == resource.get("route_selection_expression")
    if hub.tool.utils.is_running_localstack(ctx):
        # LocalStack does not have support for apigatewayv2 tags
        assert tags == resource.get("tags")
    else:
        assert new_tags == resource.get("tags")
    assert new_version == resource.get("version")

    # Search for non-existing apigatewayv2 api
    fake_resource_id = "fake-" + str(uuid.uuid4())
    ret = await hub.states.aws.apigatewayv2.api.search(
        ctx,
        name=api_temp_name,
        resource_id=fake_resource_id,
    )

    assert not ret["result"], ret["comment"]
    assert (
        f"Unable to find aws.apigatewayv2.api resource with resource id '{fake_resource_id}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and not ret.get("new_state")

    # Delete apigatewayv2 api with test flag
    ret = await hub.states.aws.apigatewayv2.api.absent(
        test_ctx,
        name=api_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.apigatewayv2.api", name=api_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete apigatewayv2 api
    ret = await hub.states.aws.apigatewayv2.api.absent(
        ctx,
        name=api_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.apigatewayv2.api", name=api_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert api_temp_name == resource.get("name")
    assert resource_id == resource.get("resource_id")
    assert protocol_type == resource.get("protocol_type")
    assert new_cors_configuration == resource.get("cors_configuration")
    assert new_description == resource.get("description")
    assert new_disable_execute_api_endpoint == resource.get(
        "disable_execute_api_endpoint"
    )
    assert route_selection_expression == resource.get("route_selection_expression")
    if hub.tool.utils.is_running_localstack(ctx):
        # LocalStack does not have support for apigatewayv2 tags
        assert tags == resource.get("tags")
    else:
        assert new_tags == resource.get("tags")
    assert new_version == resource.get("version")

    # Delete the same apigatewayv2 api again (deleted state) will not invoke delete on AWS side
    ret = await hub.states.aws.apigatewayv2.api.absent(
        ctx,
        name=api_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.api", name=api_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])

    # Delete apigatewayv2 api with no resource_id will consider it as absent
    ret = await hub.states.aws.apigatewayv2.api.absent(
        ctx,
        name=api_temp_name,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.api", name=api_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])
