import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_budget_action(
    hub,
    ctx,
    aws_iam_role_3,
    aws_organization_unit,
    aws_organization_policy_attachment,
    aws_budget,
):
    # Skipping this as Budget is not supported in localstack.
    if hub.tool.utils.is_running_localstack(ctx):
        return
    name = "idem-test-action-" + str(uuid.uuid4())
    budget_name = aws_budget.get("budget_name")
    notification_type = "ACTUAL"
    action_type = "APPLY_SCP_POLICY"
    action_threshold = {
        "ActionThresholdType": "ABSOLUTE_VALUE",
        "ActionThresholdValue": 55.0,
    }
    approval_model = "AUTOMATIC"
    policy_id = aws_organization_policy_attachment["PolicyId"]
    target_id = aws_organization_unit["resource_id"]
    execution_role_arn = aws_iam_role_3["arn"]

    definition = {
        "ScpActionDefinition": {"PolicyId": policy_id, "TargetIds": [target_id]}
    }

    subscribers = [
        {"SubscriptionType": "EMAIL", "Address": "abc@test.com"},
    ]

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create Budget Action with Test Flag
    ret = await hub.states.aws.budgets.budget_action.present(
        test_ctx,
        name=name,
        budget_name=budget_name,
        notification_type=notification_type,
        action_type=action_type,
        action_threshold=action_threshold,
        definition=definition,
        execution_role_arn=execution_role_arn,
        approval_model=approval_model,
        subscribers=subscribers,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.budgets.budget_action '{name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert budget_name == resource.get("budget_name")
    assert notification_type == resource.get("notification_type")
    assert action_type == resource.get("action_type")
    assert action_threshold == resource.get("action_threshold")
    assert definition == resource.get("definition")
    assert approval_model == resource.get("approval_model")
    assert execution_role_arn == resource.get("execution_role_arn")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        subscribers, resource.get("subscribers")
    )

    # Create Budget Action with real
    ret = await hub.states.aws.budgets.budget_action.present(
        ctx,
        name=name,
        budget_name=budget_name,
        notification_type=notification_type,
        action_type=action_type,
        action_threshold=action_threshold,
        definition=definition,
        execution_role_arn=execution_role_arn,
        approval_model=approval_model,
        subscribers=subscribers,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    assert budget_name == resource.get("budget_name")
    assert notification_type == resource.get("notification_type")
    assert action_type == resource.get("action_type")
    assert action_threshold == resource.get("action_threshold")
    assert definition == resource.get("definition")
    assert approval_model == resource.get("approval_model")
    assert execution_role_arn == resource.get("execution_role_arn")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        subscribers, resource.get("subscribers")
    )

    # Describe Budget Action
    describe_ret = await hub.states.aws.budgets.budget_action.describe(
        ctx,
    )
    assert resource_id in describe_ret
    assert "aws.budgets.budget_action.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.budgets.budget_action.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "name" in described_resource_map
    assert resource_id == described_resource_map["name"]
    assert budget_name == described_resource_map.get("budget_name")
    assert notification_type == described_resource_map.get("notification_type")
    assert action_type == described_resource_map.get("action_type")
    assert action_threshold == described_resource_map.get("action_threshold")
    assert definition == described_resource_map.get("definition")
    assert approval_model == resource.get("approval_model")
    assert execution_role_arn == resource.get("execution_role_arn")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        subscribers, described_resource_map.get("subscribers")
    )

    # Update Budget Action in test
    notification_type = "FORECASTED"
    action_threshold = {
        "ActionThresholdType": "PERCENTAGE",
        "ActionThresholdValue": 65.0,
    }
    approval_model = "MANUAL"
    subscribers = [
        {"SubscriptionType": "EMAIL", "Address": "def@test.com"},
    ]

    ret = await hub.states.aws.budgets.budget_action.present(
        test_ctx,
        name=name,
        resource_id=resource_id,
        budget_name=budget_name,
        notification_type=notification_type,
        action_type=action_type,
        action_threshold=action_threshold,
        definition=definition,
        execution_role_arn=execution_role_arn,
        approval_model=approval_model,
        subscribers=subscribers,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert notification_type == resource.get("notification_type")
    assert action_threshold == resource.get("action_threshold")
    assert approval_model == resource.get("approval_model")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        subscribers, resource.get("subscribers")
    )

    # Update Budget Action in real
    ret = await hub.states.aws.budgets.budget_action.present(
        ctx,
        name=name,
        budget_name=budget_name,
        resource_id=resource_id,
        notification_type=notification_type,
        action_type=action_type,
        action_threshold=action_threshold,
        definition=definition,
        execution_role_arn=execution_role_arn,
        approval_model=approval_model,
        subscribers=subscribers,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert notification_type == resource.get("notification_type")
    assert action_threshold == resource.get("action_threshold")
    assert approval_model == resource.get("approval_model")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        subscribers, resource.get("subscribers")
    )

    # Delete Budget Action with test flag
    ret = await hub.states.aws.budgets.budget_action.absent(
        test_ctx, name=name, budget_name=budget_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.budgets.budget_action '{name}'" in ret["comment"]
    resource = ret.get("old_state")
    assert budget_name == resource.get("budget_name")
    assert notification_type == resource.get("notification_type")
    assert action_type == resource.get("action_type")
    assert action_threshold == resource.get("action_threshold")
    assert definition == resource.get("definition")
    assert approval_model == resource.get("approval_model")
    assert execution_role_arn == resource.get("execution_role_arn")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        subscribers, resource.get("subscribers")
    )

    # Delete Budget Action in real
    ret = await hub.states.aws.budgets.budget_action.absent(
        ctx, name=name, budget_name=budget_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted aws.budgets.budget_action '{name}'" in ret["comment"]
    resource = ret.get("old_state")
    assert budget_name == resource.get("budget_name")
    assert notification_type == resource.get("notification_type")
    assert action_type == resource.get("action_type")
    assert action_threshold == resource.get("action_threshold")
    assert definition == resource.get("definition")
    assert approval_model == resource.get("approval_model")
    assert execution_role_arn == resource.get("execution_role_arn")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        subscribers, resource.get("subscribers")
    )

    # Delete Budget Action again
    ret = await hub.states.aws.budgets.budget_action.absent(
        ctx, name=name, budget_name=budget_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert f"aws.budgets.budget_action '{name}' already absent" in ret["comment"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_budget_action_absent_with_none_resource_id(hub, ctx):
    action_temp_name = "idem-test-action-" + str(uuid.uuid4())
    budget_temp_name = "idem-test-budget-" + str(uuid.uuid4())
    # Delete Budget Action with resource_id as None. Result in no-op.
    ret = await hub.states.aws.budgets.budget_action.absent(
        ctx, name=action_temp_name, budget_name=budget_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        f"aws.budgets.budget_action '{action_temp_name}' already absent"
        in ret["comment"]
    )
