import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_anomaly_subscription(hub, ctx, aws_anomaly_monitor):
    # Skipping this as Anomaly subscription is not supported in localstack and only working with real aws.
    if hub.tool.utils.is_running_localstack(ctx):
        return

    subscription_temp_name = "idem-test-anomaly-subscription-" + str(uuid.uuid4())
    monitor_arn_list = [aws_anomaly_monitor.get("resource_id")]
    subscribers = [
        {"Address": "email@test.com", "Status": "CONFIRMED", "Type": "EMAIL"}
    ]
    threshold = 5.0
    frequency = "DAILY"

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create Anomaly Subscription with Test Flag
    ret = await hub.states.aws.costexplorer.anomaly_subscription.present(
        test_ctx,
        name=subscription_temp_name,
        monitor_arn_list=monitor_arn_list,
        subscribers=subscribers,
        threshold=threshold,
        frequency=frequency,
        subscription_name=subscription_temp_name,
    )

    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.costexplorer.anomaly_subscription {subscription_temp_name}"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert subscription_temp_name == resource.get("name")
    assert monitor_arn_list == resource.get("monitor_arn_list")
    assert subscribers == resource.get("subscribers")
    assert threshold == resource.get("threshold")
    assert frequency == resource.get("frequency")

    # Create Anomaly Subscription with real
    ret = await hub.states.aws.costexplorer.anomaly_subscription.present(
        ctx,
        name=subscription_temp_name,
        monitor_arn_list=monitor_arn_list,
        subscribers=subscribers,
        threshold=threshold,
        frequency=frequency,
        subscription_name=subscription_temp_name,
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    assert subscription_temp_name == resource.get("subscription_name")
    assert monitor_arn_list == resource.get("monitor_arn_list")
    assert subscribers == resource.get("subscribers")
    assert threshold == resource.get("threshold")
    assert frequency == resource.get("frequency")

    # Describe Anomaly Subscription
    describe_ret = await hub.states.aws.costexplorer.anomaly_subscription.describe(
        ctx,
    )
    assert resource_id in describe_ret
    assert "aws.costexplorer.anomaly_subscription.present" in describe_ret.get(
        resource_id
    )
    described_resource = describe_ret.get(resource_id).get(
        "aws.costexplorer.anomaly_subscription.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "name" in described_resource_map
    assert resource_id == described_resource_map["name"]
    assert monitor_arn_list == described_resource_map.get("monitor_arn_list")
    assert subscribers == described_resource_map.get("subscribers")
    assert threshold == described_resource_map.get("threshold")
    assert frequency == described_resource_map.get("frequency")
    assert subscription_temp_name == described_resource_map.get("subscription_name")

    # Update Anomaly Subscription in test
    subscription_temp_name_update = "idem-test-anomaly-subscription-" + str(
        uuid.uuid4()
    )
    subscribers = [
        {"Address": "email2@test.com", "Status": "CONFIRMED", "Type": "EMAIL"}
    ]
    threshold = 10.0
    frequency = "WEEKLY"

    ret = await hub.states.aws.costexplorer.anomaly_subscription.present(
        test_ctx,
        name=subscription_temp_name,
        resource_id=resource_id,
        monitor_arn_list=monitor_arn_list,
        subscribers=subscribers,
        threshold=threshold,
        frequency=frequency,
        subscription_name=subscription_temp_name_update,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert subscription_temp_name_update == resource.get("subscription_name")
    assert subscribers == resource.get("subscribers")
    assert threshold == resource.get("threshold")
    assert frequency == resource.get("frequency")

    # Update Anomaly Subscription in real
    ret = await hub.states.aws.costexplorer.anomaly_subscription.present(
        ctx,
        name=subscription_temp_name,
        resource_id=resource_id,
        monitor_arn_list=monitor_arn_list,
        subscribers=subscribers,
        threshold=threshold,
        frequency=frequency,
        subscription_name=subscription_temp_name_update,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert subscription_temp_name_update == resource.get("subscription_name")
    assert subscribers == resource.get("subscribers")
    assert threshold == resource.get("threshold")
    assert frequency == resource.get("frequency")

    # Delete Anomaly Subscription with test flag
    ret = await hub.states.aws.costexplorer.anomaly_subscription.absent(
        test_ctx, name=subscription_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete aws.costexplorer.anomaly_subscription {subscription_temp_name}"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert subscription_temp_name_update == resource.get("subscription_name")
    assert subscribers == resource.get("subscribers")
    assert threshold == resource.get("threshold")
    assert frequency == resource.get("frequency")

    # Delete Anomaly Subscription in real
    ret = await hub.states.aws.costexplorer.anomaly_subscription.absent(
        ctx, name=subscription_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Deleted aws.costexplorer.anomaly_subscription '{subscription_temp_name}'"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert subscription_temp_name_update == resource.get("subscription_name")
    assert subscribers == resource.get("subscribers")
    assert threshold == resource.get("threshold")
    assert frequency == resource.get("frequency")

    # Delete Anomaly Subscription again
    ret = await hub.states.aws.costexplorer.anomaly_subscription.absent(
        ctx, name=subscription_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"aws.costexplorer.anomaly_subscription '{subscription_temp_name}' already absent"
        in ret["comment"]
    )
